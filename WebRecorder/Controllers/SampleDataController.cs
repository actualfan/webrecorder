using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using WebRecorder.RestModel;

namespace WebRecorder.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        private IHostingEnvironment _hostingEnvironment;

        public SampleDataController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost("[action]")]
        public async Task<string> UploadAudio([FromForm] UploadAudioRequest audioFile)
        {
            string url = "";

            // Copy file first
            string relativePath = null;
            if (audioFile.RecordedMedia != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(Path.GetRandomFileName());
                string fileExt = "mp3";
                string fullName = fileName + "." + fileExt;

                string folderName = "Upload";
                string webRootPath = _hostingEnvironment.WebRootPath;
                if (string.IsNullOrWhiteSpace(webRootPath))
                {
                    webRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                }
                string imagePath = Path.Combine(webRootPath, folderName);
                string imageUrl = Path.Combine(imagePath, fullName);
                relativePath = Path.Combine(folderName, fullName);
                if (!Directory.Exists(imagePath))
                {
                    Directory.CreateDirectory(imagePath);
                }
                using (var stream = new FileStream(imageUrl, FileMode.Create))
                {
                    await audioFile.RecordedMedia.CopyToAsync(stream);
                }
            }

            return relativePath;
        }
    }
}
