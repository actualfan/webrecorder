import React, { Component } from 'react';
import { Col, Grid, Row, Glyphicon, Button } from 'react-bootstrap';

export class Home extends Component {
  displayName = Home.name

  render() {
    return (
        <Grid fluid className='v-center-grid-container home-container'>
            <Row>
                <Col xsOffset={1} smOffset={1} mdOffset={2} lgOffset={3} xs={10} sm={5} md={4} lg={3}>
                    <Button href='/dashboard/record/practice' className='block'>
                        <p>PRACTICE MODE</p>
                        <Glyphicon glyph="book" className='ico' />
                    </Button>
                </Col>
                <Col xsOffset={1} smOffset={0} xs={10} sm={5} md={4} lg={3}>
                    <Button href='/dashboard/record/timed' className='block'>
                        <p>TIMED MODE</p>
                        <Glyphicon glyph="time" className='ico' />
                    </Button>
                </Col>
            </Row>
        </Grid>
    );
  }
}
