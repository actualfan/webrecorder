import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './Layout';
import { RecordPage } from './RecordPage';

export class Dashboard extends Component {
    displayName = Dashboard.name

    render() {
        return (
            <Layout>
                <Route path='/dashboard/record/:mode' component={RecordPage} />
            </Layout>
        );
    }
}
