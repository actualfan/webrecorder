import React, { Component } from 'react';
import { AudioRecordPlay } from './AudioRecordPlay';

export class RecordPage extends Component {
    state = { completed: false };

    componentWillReceiveProps(newProps) {
        if (newProps.match.params.mode !== this.props.match.params.mode) {
            this.setState({ completed: false });
        }
    }

    onUploadComplete = () => {
        this.setState({ completed: true });
    }

    render() {
        const { completed } = this.state;
        const mode = this.props.match.params.mode ? this.props.match.params.mode : 'practice';
        const modeTitle = mode === 'practice' ? 'Practice Mode' : 'Timed Mode';
        const subText = 'Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. Temporary texts. ';
        const footText = 'Basic text. Basic textBasic textBasic textBasic textBasic textBasic textBasic textBasic textBasic textBasic textBasic textBasic textBasic textBasic textBasic text';
        return (
            <div>
                <div className='row mt-16'>
                    <div className='col-12'>
                        <h2>Recording Page ({modeTitle})</h2>
                        <p>{subText}</p>
                    </div>
                </div>
                <div className='row mt-16 justify-content-center'>
                    <div className='col-xs-12 col-sm-8 col-md-6'>
                        <AudioRecordPlay
                            mode={mode}
                            onUploadComplete={this.onUploadComplete}
                        />
                    </div>
                </div>
                <div className='row mt-16 relative-container'>
                    <p>{footText}</p>
                </div>
                <div className='row mt-16 relative-container'>
                    {completed && <button className='btn btn-primary pull-right mr-32'>Next</button>}
                </div>
            </div>
        );
    }
}
