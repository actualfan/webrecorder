﻿import React, { Component } from 'react';
import vmsg from "../assets/js/vmsg";
import { sendRequest } from '../assets/js/apiRequest';
import { Glyphicon, ProgressBar } from 'react-bootstrap';
import { Slider, Direction } from 'react-player-controls';

const recorder = new vmsg.Recorder({
    wasmURL: "https://unpkg.com/vmsg@0.3.0/vmsg.wasm"
});

export class AudioRecordPlay extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isRecording: false,
            isPlaying: 0,
            elapsedTime: 0,
            totalSeconds: this.maxTime,
            tid: 0,
            recordedMedia: null
        };

        this.audio.addEventListener('loadeddata', () => {
            const totalSeconds = Math.floor(this.audio.duration + 0.5);
            clearTimeout(this.tid);
            this.setState({
                elapsedTime: 0,
                totalSeconds
            });
        });

        this.audio.addEventListener('ended', () => {
            this._stopAudio();
        });

        if (props.mode === 'timed') {
            this.state.info = '';
        } else {
            this.state.info = 'Ready';
        }
    }

    componentDidMount() {
        if (this.props.mode === 'timed') {
            this.prepareSecondCount = this.prepareTime;
            this._autoRecord();
        }
    }

    async componentWillReceiveProps(newProps) {
        if (newProps.mode !== this.props.mode) {
            clearTimeout(this.tid);
            if (this.state.isRecording) {
                await this._stopRecord('');
            }
            if (this.state.isPlaying === 2) {
                if (this.audio) {
                    this.audio.pause();
                }
            }
            this.setState({
                isLoading: false,
                isRecording: false,
                isPlaying: 0,
                elapsedTime: 0,
                totalSeconds: this.maxTime,
                tid: 0,
                recordedMedia: null,
                info: 'Ready'
            });
            if (newProps.mode === 'timed') {
                this.prepareSecondCount = this.prepareTime;
                this._autoRecord();
            }
        }
    }

    startTime = 0;
    audio = new Audio();
    maxTime = 40;
    prepareTime = 25;
    
    _autoRecord = () => {
        var info = '';
        if (this.prepareSecondCount > 1) {
            info = `Recording in ${this.prepareSecondCount} seconds`;
        } else if (this.prepareSecondCount === 1) {
            info = `Recording in a second`;
        } else if (this.prepareSecondCount === 0) {
            info = 'Ready';
            clearTimeout(this.tid);
            this._startRecord();
            return;
        }

        this.tid = setTimeout(() => this._autoRecord(), 1000);
        this.setState({ info });
        this.prepareSecondCount = this.prepareSecondCount - 1;
    }

    _autoPlay = () => {
        var info = '';
        if (this.prepareSecondCount > 1) {
            info = `Play in ${this.prepareSecondCount} seconds`;
        } else if (this.prepareSecondCount === 1) {
            info = `Play in a second`;
        } else if (this.prepareSecondCount === 0) {
            info = 'Play';
            clearTimeout(this.tid);
            this._playAudio();
            return;
        }

        this.tid = setTimeout(() => this._autoPlay(), 1000);
        this.setState({ info });
        this.prepareSecondCount = this.prepareSecondCount - 1;
    }

    _onSlience = () => {
        this._stopRecord('Recording stopped due to 3 seconds of silence');
    }

    _startRecord = async () => {
        this.setState({ isLoading: true });
        try {
            await recorder.initAudio();
            if (this.props.mode === 'timed') {
                recorder.listenSilence(this._onSlience);
            }
            await recorder.initWorker();
            recorder.startRecording();
            this.startTime = Date.now();
            this.setState({
                isLoading: false,
                isRecording: true,
                elapsedTime: 0,
                recordedMedia: null,
                totalSeconds: this.maxTime,
                info: 'Recording'
            });
            this.updateTime();
        } catch (e) {
            console.error(e);
            this.setState({ isLoading: false });
        }
    }

    _stopRecord = async (status = 'Uploading audio file') => {
        this.setState({ isLoading: true });
        const blob = await recorder.stopRecording();
        clearTimeout(this.tid);
        const recordedMedia = URL.createObjectURL(blob);
        this.audio.src = recordedMedia;
        this.setState({
            isLoading: false,
            isRecording: false,
            recordedMedia,
            info: status
        });
        await this._uploadAudio();
        this.setState({
            info: 'Completed'
        });
        if (this.props.mode === 'timed') {
            this.prepareSecondCount = this.prepareTime;
            this._autoPlay();
        }
        if (this.props.onUploadComplete) {
            this.props.onUploadComplete();
        }
    }

    _uploadAudio = async () => {
        if (!recorder.blob) {
            return;
        }
        const res = await sendRequest('api/SampleData/UploadAudio', { recordedMedia: recorder.blob }, 'post', 'multipart/form-data');
        return res;
    }

    _playAudio = () => {
        if (this.state.recordedMedia) {
            this.audio.play();
            this.setState({ isPlaying: 2, info: 'Play' });
            this.updateTime();
        }
    }

    _pauseAudio = () => {
        this.audio.pause();
        this.setState({ isPlaying: 1, info: 'Pause' });
        clearTimeout(this.tid);
    }

    _stopAudio = () => {
        this.audio.pause();
        this.audio.src = this.state.recordedMedia;
        clearTimeout(this.tid);
        this.setState({ isPlaying: 0, info: 'Ready' });
    }

    updateTime = () => {
        var elapsedTime = 0;
        if (this.state.isRecording) {
            elapsedTime = (Date.now() - this.startTime) / 1000;
            if (elapsedTime > this.maxTime) {
                this._stopRecord('Recording stopped due to maximum length reached');
            }
        } else if (this.state.isPlaying === 2) {
            elapsedTime = this.audio.currentTime;
        }

        this.setState({ elapsedTime });
        this.tid = setTimeout(() => this.updateTime(), 300);
    }

    onRecordButtonClick = () => {
        if (this.state.isPlaying) {
            this._stopAudio();
        } else if (this.state.isRecording) {
            this._stopRecord();
        } else {
            this._startRecord();
        }
    };

    onPlayButtonClick = () => {
        if (this.state.isPlaying === 2) {
            this._pauseAudio();
        } else {
            this._playAudio();
        }
    }

    onChangePlayPosition = (newValue) => {
        const { isRecording, recordedMedia, totalSeconds } = this.state;
        if (isRecording || !recordedMedia) {
            return;
        }
        const newSecond = newValue * totalSeconds;
        this.audio.currentTime = newSecond;
        this.setState({
            elapsedTime: newSecond
        });
    }

    render() {
        const { isLoading, isRecording, isPlaying, elapsedTime, totalSeconds, info, recordedMedia } = this.state;
        const { mode } = this.props;
        
        const elapsedSeconds = Math.floor(elapsedTime + 0.5);
        const elapsedTimeNote = `00:${elapsedSeconds < 10 ? '0' + elapsedSeconds : elapsedSeconds} / 00:${totalSeconds < 10 ? '0' + totalSeconds : totalSeconds}`;
        const elapsedPercents = elapsedTime / totalSeconds * 100;
        
        const isRecordDisabled = mode === 'timed' || isLoading;
        const isPlayDisabled = mode === 'timed' || isLoading || isRecording || !recordedMedia;

        const recordButtonGlyph = (isRecording || isPlaying) ? 'stop' : 'record';
        const playButtonGlyph = isPlaying === 2 ? 'pause' : 'play';

        const recordButtonClass = (isRecording || isPlaying) ? '' : 'record';

        return (
            <div className='audio-record-play-container'>
                <p className='title'>Audio Recorder</p>
                <div className='info-container'>
                    <label>{info}</label>
                </div>
                <div className='player'>
                    <button className={recordButtonClass} disabled={isRecordDisabled} onClick={this.onRecordButtonClick}>
                        <Glyphicon glyph={recordButtonGlyph} />
                    </button>
                    <div className='border'/>
                    <button disabled={isPlayDisabled} onClick={this.onPlayButtonClick}>
                        <Glyphicon glyph={playButtonGlyph} />
                    </button>
                    <div className='border' />
                    <span className='time-title'>{elapsedTimeNote}</span>
                    <Slider
                        isEnabled={this.state.isEnabled}
                        direction={this.state.direction}
                        onChange={this.onChangePlayPosition}
                        className='play-bar'
                    >
                        <ProgressBar now={elapsedPercents} className='play-bar' />
                    </Slider>
                </div>
            </div>
        );
    }
}
