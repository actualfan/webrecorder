import React, { Component } from 'react';
import { Route } from 'react-router';
import { Home } from './components/Home';
import { Dashboard } from './components/Dashboard';

export default class App extends Component {
    displayName = App.name

    render() {
        return (
            <div>
                <Route exact path='/' component={Home} />
                <Route path='/dashboard' component={Dashboard} />
            </div>
        );
    }
}
