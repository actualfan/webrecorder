import fetch from 'cross-fetch';
//import { ReduxStore } from '../App.js';

// function timeout(ms, promise) {
//     return new Promise(function(resolve, reject) {
//         setTimeout(function() {
//             reject(new Error("timeout"))
//         }, ms)
//         promise.then(resolve, reject)
//     })
// }

/**
 * Serialize javascript object for sending to api
 * @param {Object} data
 * @returns {String}
 */
function serialize(data) {
    return Object.keys(data).map((keyName) => {
        return `${encodeURIComponent(keyName)}=${data[keyName] ? encodeURIComponent(data[keyName]) : ''}`;
    }).join('&');
}

/**
 * Method for making ajax calls to the site's api
 * @param {String} endpoint - the api endpoint
 * @param {Object|string} [data=null] - key:value pairs of the data to be sent to server
 * @param {String} [method=get] - the type of ajax request to make
 * @param {String} [contentType=json] - content type to send
 * @returns {Promise}
 */
export async function sendRequest(endpoint, data = null, method = 'get', contentType = 'application/json') {
    let url = `${process.env.REACT_APP_API_URL}${endpoint}`;

    url = (method === 'get' || method === 'delete') && data !== null ? `${url}?${serialize(data)}` : url;
 
    var headers = {
        'Content-Type': contentType
    }
    
    var body = null
    if (contentType.indexOf('x-www-form-urlencoded') !== -1 && data) {
        var formBody = [];
        for (var property in data) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(data[property]);
        formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");
        body = formBody
    } else if (contentType.indexOf('multipart/form-data') !== -1 && data) {
        headers = {}
        body = new FormData()
        for (var name in data) {
            body.append(name, data[name])
        }
    } else {
        body = data === null || (method === 'get' || method === 'delete') ? null : (contentType === 'application/json' ? JSON.stringify(data) : data)
    }

    //const reduxState = ReduxStore.getState()
    //if (reduxState.auth.profile.apiToken) {
    //    const token = reduxState.auth.profile.apiToken.token
    //    if (token) {
    //        headers.Authorization = 'Bearer ' + token
    //    }
    //}

    try {
        const response = await fetch(url, {
            method,
            body,
            headers
        });

        // const response = await timeout(Api.timeInterval, fetch(url, {
        //     method,
        //     headers,
        //     credentials: 'include',
        //     body
        // }));
        
        if (response.ok) {
            const receivedContentType = response.headers.get('content-type');

            if (receivedContentType && receivedContentType.indexOf('application/json') === -1) {

                // sometimes json comes with text/html content-type
                const data = await response.text();

                try {
                    return response.status === 204 ? {status: true} : {status: true, data: JSON.parse(data)};
                }
                catch (err) {

                    // handle the case when the data is not json but html
                    return {status: false, message: data}
                }
            }

            return response.status === 204 ? {status: true} : {status: true, data: await response.json()};
        }
        else {
            const data = await response.text()
            const message = data && data.message ? data.message : "Invalid api call"
            return {status: false, message}
        }
    } catch (error) {
        return {status: false, message: "Time out"}
    }
}