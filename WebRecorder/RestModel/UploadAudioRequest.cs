﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebRecorder.RestModel
{
    public class UploadAudioRequest
    {
        public IFormFile RecordedMedia { get; set; }
    }
}
